## [2022-07-21]
+ First Effect: Explosion

## [2022-05-29]
+ Optimized joystick
+ Controls virtual joystick by mouse (touch emulated by mouse)

## [2022-05-28]
+ Toggle joysticks in HUD (J)
+ Destructible towers by killing enemies spawned at more random locations
+ New Input action ui_toggle_joysticks

## [2022-05-26]
+ generator: Trees always on the ground

## [2022-05-25]
+ speed submenu with checkboxes

## [2022-05-24]
+ facebook, twitter, youtube channel

## [2022-05-23]
+ many buttons
+ delete save
+ saves & abilities submenu