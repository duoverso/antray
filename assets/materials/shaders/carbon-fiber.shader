// https://godotshaders.com/shader/carbon-fiber-wrap/
shader_type spatial;

const float shade = 0.1;
uniform float reflection = 1.0;
uniform float scale_control = 1.0;

void fragment(){
	float scale = scale_control * 50.0;
	
	float y = UV.y * scale;
	float x = UV.x * scale;
	int surface = int( float(int(y)) / (2.0/5.0) +x ) % 2;
	
	ALBEDO = vec3(shade, shade, shade);
	ROUGHNESS = 0.65;
	METALLIC = 0.75;
	
	if(surface == 1){
		NORMALMAP = vec3(sin(UV.x*scale*reflection*0.1), 1.0, 1.0);
	}else{
		NORMALMAP = vec3(cos(UV.y*scale*reflection*0.1), 1.0, 1.0);
	}
}