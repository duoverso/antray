extends Node

var logchan := "sound"

var settings_section := "sound"
var settings_properties := ["bus_volume"]

export var bus_name := "Sound"
var bus := AudioServer.get_bus_index(bus_name)
export var bus_volume: float setget set_bus_volume # 0 to 1.0

var stream_player := preload("res://core/audio/stream_player.tscn")
var stream_player_2d := preload("res://core/audio/stream_player_2d.tscn")
var stream_player_3d := preload("res://core/audio/stream_player_3d.tscn")

var breaking_wood := preload("res://assets/audio/sound/breaking-wood.wav")

#var w3d_x1 := preload("res://assets/audio/sound/wolf3d/1.wav")
#var w3d_woof := preload("res://assets/audio/sound/wolf3d/2.wav")
var w3d_door_opening := preload("res://assets/audio/sound/wolf3d/3.wav")
var w3d_punch := preload("res://assets/audio/sound/wolf3d/4.wav")
var w3d_pistol_shot := preload("res://assets/audio/sound/wolf3d/5.wav")
#var w3d_rifle_shot := preload("res://assets/audio/sound/wolf3d/6.wav")
#var w3d_x2 := preload("res://assets/audio/sound/wolf3d/7.wav")
#var w3d_guten_tag := preload("res://assets/audio/sound/wolf3d/8.wav")
#var w3d_muutii := preload("res://assets/audio/sound/wolf3d/9.wav")
#var w3d_salvo := preload("res://assets/audio/sound/wolf3d/10.wav")
#var w3d_machine_gun := preload("res://assets/audio/sound/wolf3d/11.wav")
#var w3d_halt := preload("res://assets/audio/sound/wolf3d/12.wav")
#var w3d_aii := preload("res://assets/audio/sound/wolf3d/13.wav")
var w3d_oh := preload("res://assets/audio/sound/wolf3d/14.wav")
#var w3d_secret_opening := preload("res://assets/audio/sound/wolf3d/15.wav")
#var w3d_woof2 := preload("res://assets/audio/sound/wolf3d/16.wav")
var w3d_aah := preload("res://assets/audio/sound/wolf3d/17.wav")
#var w3d_x3 := preload("res://assets/audio/sound/wolf3d/18.wav")
#var w3d_x4 := preload("res://assets/audio/sound/wolf3d/19.wav")
#var w3d_x5 := preload("res://assets/audio/sound/wolf3d/20.wav")
var w3d_rifle_shot2 := preload("res://assets/audio/sound/wolf3d/21.wav")
var w3d_bubbling := preload("res://assets/audio/sound/wolf3d/22.wav")
#var w3d_x6 := preload("res://assets/audio/sound/wolf3d/23.wav")
#var w3d_x7 := preload("res://assets/audio/sound/wolf3d/24.wav")
#var w3d_haha := preload("res://assets/audio/sound/wolf3d/25.wav")
#var w3d_haha2 := preload("res://assets/audio/sound/wolf3d/26.wav")
#var w3d_spion := preload("res://assets/audio/sound/wolf3d/27.wav")
#var w3d_x8 := preload("res://assets/audio/sound/wolf3d/28.wav")
#var w3d_woof_woof := preload("res://assets/audio/sound/wolf3d/29.wav")
var w3d_clink := preload("res://assets/audio/sound/wolf3d/30.wav")
var w3d_clank := preload("res://assets/audio/sound/wolf3d/31.wav")
var w3d_yeaah := preload("res://assets/audio/sound/wolf3d/32.wav")
#var w3d_x9 := preload("res://assets/audio/sound/wolf3d/33.wav")
#var w3d_aii2 := preload("res://assets/audio/sound/wolf3d/34.wav")
var w3d_oouh := preload("res://assets/audio/sound/wolf3d/35.wav")
#var w3d_dona_veta := preload("res://assets/audio/sound/wolf3d/36.wav")
#var w3d_x10 := preload("res://assets/audio/sound/wolf3d/37.wav")
#var w3d_x11 := preload("res://assets/audio/sound/wolf3d/38.wav")
#var w3d_fart := preload("res://assets/audio/sound/wolf3d/39.wav")
#var w3d_haah := preload("res://assets/audio/sound/wolf3d/40.wav")
#var w3d_huuh := preload("res://assets/audio/sound/wolf3d/41.wav")
var w3d_uuh := preload("res://assets/audio/sound/wolf3d/42.wav")
#var w3d_x12 := preload("res://assets/audio/sound/wolf3d/43.wav")
#var w3d_x13 := preload("res://assets/audio/sound/wolf3d/44.wav")
#var w3d_x14 := preload("res://assets/audio/sound/wolf3d/45.wav")

var ut_bio_rifle_raise := preload("res://assets/audio/sound/unreal/weapon/bio-rifle/raise.wav")
var ut_bio_rifle_primary := preload("res://assets/audio/sound/unreal/weapon/bio-rifle/primary.wav")

var ut_chainsaw_raise := preload("res://assets/audio/sound/unreal/weapon/chainsaw/raise.wav")
var ut_chainsaw_primary := preload("res://assets/audio/sound/unreal/weapon/chainsaw/primary.wav")

var ut_enforcer_raise := preload("res://assets/audio/sound/unreal/weapon/enforcer/raise.wav")
var ut_enforcer_primary := preload("res://assets/audio/sound/unreal/weapon/enforcer/primary.wav")

var ut_flak_raise := preload("res://assets/audio/sound/unreal/weapon/flak/raise.wav")
var ut_flak_primary := preload("res://assets/audio/sound/unreal/weapon/flak/primary.wav")

var ut_impact_hammer_raise := preload("res://assets/audio/sound/unreal/weapon/impact-hammer/raise.wav")
var ut_impact_hammer_primary := preload("res://assets/audio/sound/unreal/weapon/impact-hammer/primary.wav")

var ut_minigun_raise := preload("res://assets/audio/sound/unreal/weapon/minigun/raise.wav")
var ut_minigun_primary := preload("res://assets/audio/sound/unreal/weapon/minigun/primary.wav")
var ut_minigun_secondary := preload("res://assets/audio/sound/unreal/weapon/minigun/secondary.wav")

var ut_pulse_gun_raise := preload("res://assets/audio/sound/unreal/weapon/pulse-gun/raise.wav")
var ut_pulse_gun_primary := preload("res://assets/audio/sound/unreal/weapon/pulse-gun/primary.wav")
var ut_pulse_gun_secondary := preload("res://assets/audio/sound/unreal/weapon/pulse-gun/secondary.wav")

var ut_redeemer_raise := preload("res://assets/audio/sound/unreal/weapon/redeemer/raise.wav")
var ut_redeemer_explode := preload("res://assets/audio/sound/unreal/weapon/redeemer/explode.wav")
var ut_redeemer_primary := preload("res://assets/audio/sound/unreal/weapon/redeemer/primary.wav")

var ut_ripper_raise := preload("res://assets/audio/sound/unreal/weapon/ripper/raise.wav")
var ut_ripper_primary := preload("res://assets/audio/sound/unreal/weapon/ripper/primary.wav")
var ut_ripper_secondary := preload("res://assets/audio/sound/unreal/weapon/ripper/secondary.wav")

var ut_rocket_launcher_raise := preload("res://assets/audio/sound/unreal/weapon/rocket-launcher/raise.wav")
var ut_rocket_launcher_primary := preload("res://assets/audio/sound/unreal/weapon/rocket-launcher/primary.wav")
var ut_rocket_launcher_secondary := preload("res://assets/audio/sound/unreal/weapon/rocket-launcher/secondary.wav")

var ut_shock_rifle_raise := preload("res://assets/audio/sound/unreal/weapon/shock-rifle/raise.wav")
var ut_shock_rifle_primary := preload("res://assets/audio/sound/unreal/weapon/shock-rifle/primary.wav")
var ut_shock_rifle_secondary := preload("res://assets/audio/sound/unreal/weapon/shock-rifle/secondary.wav")

var ut_sniper_raise := preload("res://assets/audio/sound/unreal/weapon/sniper/raise.wav")
var ut_sniper_primary := preload("res://assets/audio/sound/unreal/weapon/sniper/primary.wav")

var ut_adrum1 := preload("res://assets/audio/sound/unreal/adrum1.wav")

var default_weapon_raise := ut_sniper_raise
var weapon_raise

var default_weapon_primary := ut_enforcer_primary # w3d_pistol_shot
var weapon_primary
var weapon_primary_player # for looped sounds

var default_weapon_primary_loop := false
var weapon_primary_loop

func set_bus_volume(new_volume: float) -> void:
	Log.verbose("set_bus_volume(" + str(new_volume) + ")", logchan)
	bus_volume = new_volume
	AudioServer.set_bus_volume_db(bus, linear2db(new_volume))

func _enter_tree():
	Log.verbose("_enter_tree() at: " + get_path(), logchan)
	Settings.connect("settings_loaded", self, "load_settings")
	Log.verbose("Settings.connect(settings_loaded) to load_settings()", logchan)
	Settings.connect("save_settings", self, "save_settings")
	Log.verbose("Settings.connect(save_settings) to save_settings()", logchan)
	bus_volume = db2linear(AudioServer.get_bus_volume_db(bus))

func load_settings() -> void:
	Settings.load_to(self, settings_section, settings_properties)

func save_settings() -> void:
	Settings.save_from(self, settings_section, settings_properties)

func _ready():
	Log.verbose("_ready()", logchan)
	load_settings()
	update_weapon_by_default()

func str_sample_info(sample) -> String:
	if not sample is AudioStreamSample:
		return "not-sample"
	var size: int = sample.data.size()
	if size <= 0:
		return "no-data"
	return str(size) + " B " \
		+ "(" + str(sample.mix_rate) + " Hz" \
		+ (", loop" if sample.loop_mode != AudioStreamSample.LOOP_DISABLED else "") \
		+ ")"

func reset_weapon():
	weapon_raise = null
	stop_weapon_primary()
	weapon_primary = null
	weapon_primary_loop = null

func update_weapon_by_default():
	if weapon_raise == null:
		weapon_raise = default_weapon_raise
	if weapon_primary == null:
		weapon_primary = default_weapon_primary
	if weapon_primary_loop == null:
		weapon_primary_loop = default_weapon_primary_loop
	Log.verbose("weapon_raise = " + str_sample_info(weapon_raise) + ", " \
		+ "weapon_primary = " + str_sample_info(weapon_primary), logchan)

# target: null = Global.player
# as_child: true = add as child to target; false = add as child to scene root at target.global_transform.origin
func play(stream: AudioStreamSample, target = null, as_child := true) -> AudioStreamPlayer:
	var player #: AudioStreamPlayer|AudioStreamPlayer3D
	if target:
		player = stream_player_3d.instance()
	else:
		player = stream_player.instance()

	player.stream = stream

	if !(player is AudioStreamPlayer3D):
		#Log.debug("play(not3D, " + str(target) + ", " + str(as_child) + ")", logchan)
		#get_tree().root.get_children()[0].add_child(player)
		add_child(player)
	else:
		if target == null:
			target = Global.player

		if as_child:
			#Log.debug("play(3D, " + str(target) + ", " + str(as_child) + ")", logchan)
			target.add_child(player)
		else:
			#Log.debug("play(3D, " + str(get_tree().root.get_children()[0]) + ", " + str(as_child) + ")", logchan)
			get_tree().root.get_children()[0].add_child(player)
			player.global_transform.origin = target.global_transform.origin

	player.play()
	return player

func play_bubbling(target = null, as_child := true) -> AudioStreamPlayer:
	return play(w3d_bubbling, target, as_child)

func play_weapon_raise(target = null, as_child := true) -> AudioStreamPlayer:
	return play(weapon_raise, target, as_child)

func play_oh(target = null, as_child := true) -> AudioStreamPlayer:
	return play(w3d_oh, target, as_child)

func play_clink(target = null, as_child := true) -> AudioStreamPlayer:
	return play(w3d_clink, target, as_child)

func play_clank(target = null, as_child := true) -> AudioStreamPlayer:
	return play(w3d_clank, target, as_child)

func play_door_opening(target = null, as_child := true) -> AudioStreamPlayer:
	return play(w3d_door_opening, target, as_child)

func play_punch(target = null, as_child := true) -> AudioStreamPlayer:
	return play(w3d_punch, target, as_child)

func play_weapon_primary(target = null, as_child := true):
	Log.verbose("weapon_primary(" + str(target) + ", " + str(as_child) + ")", logchan)
	if weapon_primary_loop:
		if not weapon_primary_player:
			weapon_primary_player = play(weapon_primary, target, as_child)
	else:
		play(weapon_primary, target, as_child)

func stop_weapon_primary():
	if weapon_primary_player:
		weapon_primary_player.stop()
		weapon_primary_player = null

func play_underwater_no_breath(target = null, as_child := true) -> AudioStreamPlayer:
	return play(w3d_uuh, target, as_child)

func play_enemy_spawned(target = null, as_child := true) -> AudioStreamPlayer:
	return play(w3d_yeaah, target, as_child)

func play_enemy_hit(target = null, as_child := true) -> AudioStreamPlayer:
	return play(w3d_oouh, target, as_child)

func play_enemy_died(target = null, as_child := true) -> AudioStreamPlayer:
	return play(w3d_aah, target, as_child)

func play_tower_entered(target = null, as_child := true) -> AudioStreamPlayer:
	return play(ut_adrum1, target, as_child)

func play_tower_destroyed(target = null, as_child := true) -> AudioStreamPlayer:
	return play(ut_redeemer_explode, target, as_child)
