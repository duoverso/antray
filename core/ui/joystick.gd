extends TouchScreenButton
### Joystick
# - can be scaled
#
# Thanks to:
# Gonkee> Godot 3: How to make a Joystick (https://youtu.be/uGyEP2LUFPg)
# Mokarrom Hossain> Joystick in Godot [in 3 minutes] (https://www.youtube.com/watch?v=wbTdRJacCMA)

var logchan := "joystick"

export var analog := true

export var threshold := 10.0
export var use_threshold_x := false
export var threshold_x := 0.05 # normalized
export var use_threshold_y := false
export var threshold_y := 0.05 # normalized
export var return_accel := 20.0

var touch_idx := -1

onready var radius := Vector2(shape.radius, shape.radius) # Vector2(30, 30)
onready var max_distance: float = get_parent().texture.get_width() / 2 # 70
onready var scene_tree := get_tree()
onready var parent_global_position: Vector2 = get_parent().get_global_position()

#func _ready():
#	set_process(false)
#	set_process_input(false)
#	set_process_unhandled_input(false)
#	set_process_internal(false)
#	set_physics_process(false)

func _process(delta):
	if touch_idx == -1:
		var pos_difference = -radius - position
		position += pos_difference * return_accel * delta

func get_center_position() -> Vector2:
	return position + radius

#func get_value() -> Vector2:
#	var pos = get_center_position()
#	var length = pos.length()
#	if length > threshold:
#		return pos.normalized()
#	return Vector2(0, 0)

func get_value() -> Vector2:
	var center_position = get_center_position()
	var length = center_position.length()

	if length < threshold:
		return Vector2() # faster than Vector2.ZERO and Vector2(0, 0) ?

	var value: Vector2 = center_position.normalized()
	if length < max_distance:
	#	Log.debug("over threshold: " + str(length), logchan)
		value *= (length / max_distance)
	#else:
	#	Log.debug("over boundary: " + str(length), logchan)

	if use_threshold_x and abs(value.x) < threshold_x:
		value.x = 0.0
	if use_threshold_y and abs(value.y) < threshold_y:
		value.y = 0.0

	return value

func _unhandled_input(event: InputEvent) -> void:
	#Log.verbose("_unhandled_input( " + str(event) + " )", logchan)
	# Start and finish drag
#	if event is InputEventScreenTouch:
#		# Start drag
#		if event.is_pressed():
#			touch_idx = event.index
#
#		# Finish drag... !event.is_pressed() = touch released
#		elif event.index == touch_idx:
#			touch_idx = -1
#	# Drag continues
#	elif event is InputEventScreenDrag:
#		pass

	if event is InputEventScreenDrag or (event is InputEventScreenTouch and event.is_pressed()):
		var position_scaled: Vector2 = (event.position - parent_global_position) / global_scale
		#var distance = (event.position - get_parent().get_global_position()).length()
		#var distance = event.position.distance_to(parent_global_position)

		#if distance <= max_distance_scaled or event.index == touch_idx:
		if position_scaled.length() <= max_distance or event.index == touch_idx:
			#set_global_position(event.position - radius_scaled)
			#set_position((event.position - parent_global_position).limit_length(max_distance) - radius_scaled)
			set_position(position_scaled.limit_length(max_distance) - radius)
			#var new_position = (event.position - parent_global_position - radius_scaled).limit_length(max_distance)
			#set_position(new_position - radius)

			#if get_center_position().length() > max_distance:
			#	set_position(get_center_position().normalized() * max_distance - radius)

			touch_idx = event.index
			Log.debug("partialy handled " + str(event), logchan)
			#scene_tree.set_input_as_handled()

	elif event is InputEventScreenTouch and !event.is_pressed() and event.index == touch_idx:
		touch_idx = -1
		Log.debug("partialy handled " + str(event), logchan)
		scene_tree.set_input_as_handled()
