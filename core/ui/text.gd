extends Node

var logchan := "text"

func _enter_tree():
	Log.verbose("_enter_tree() at: " + get_path(), logchan)

func label3d(parent: Spatial, text: String, options := {}) -> void:
	var label := Label3D.new()
	label.text = text
	if options.has("name"):
		label.name = options["name"] # "Label3D"
	if options.has("pixel_size"):
		label.pixel_size = options["pixel_size"] # 0.5
	if options.has("billboard"):
		label.billboard = options["billboard"] # true
	if options.has("modulate"):
		label.modulate = options["modulate"] # Color(0, 0, 0)
	if options.has("outline_modulate"):
		label.outline_modulate = options["outline_modulate"] # Color(1, 1, 1)
	if options.has("translation"):
		label.translation = options["translation"]
	parent.add_child(label)

func billboard(parent: Spatial, text: String, options := {}) -> void:
	options["billboard"] = true
	label3d(parent, text, options)
