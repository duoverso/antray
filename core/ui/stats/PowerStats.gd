# Power System
# for better balanced health, damage, reward and parent power

#class_name PowerStats
extends Stats

# Use parent.pre_stats for power < 0
export var power: float = -1

# Called by Stats._ready()
func init_data() -> void:
	#Log.verbose(str(parent) + " -- PowerStats::init_data()", parent.logchan)
	#health = max(0.1, power * lerp(0.333, 0.666, randf()))
	reinit_power()
	#Log.debug(str(parent) + ".stats.init_data(): power=" + str(power) + ", health=" + str(health) + " / " + str(max_health) + ", damage=" + str(damage) + ", reward=" + str(reward), parent.logchan)
	#Log.debug(str(parent) + ".stats.init_data()", parent.logchan)

func init_power(_power: float) -> void:
	power = _power
	set_max_health(power * 0.5)
	#set_health(max_health)
	set_health_to_max()
	damage = power - max_health
	reward = int(power)
	#Log.verbose(str(parent) + " ++ PowerStats::init_power(" + str(_power) + "): power=" + str(power) + ", health=" + str(health) + " / " + str(max_health) + ", damage=" + str(damage) + ", reward=" + str(reward), parent.logchan)

func reinit_power() -> void:
	#Log.verbose(str(parent) + " -- PowerStats::reinit_power(" + str(power) + ")", parent.logchan)
	if power < 0:
		if "pre_stats" in parent and "power" in parent.pre_stats:
			power = parent.pre_stats["power"]
		else:
			power = 0
	init_power(power)
