# https://docs.godotengine.org/en/stable/tutorials/networking/high_level_multiplayer.html
extends SceneTree

const SERVER_PORT = 30123
const MAX_PLAYERS = 8

func _init():
	print("(server-test) _init()")
	connect("network_peer_connected", self, "_peer_connected")
	connect("network_peer_disconnected", self, "_peer_disconnected")

	var peer = NetworkedMultiplayerENet.new()
	peer.create_server(SERVER_PORT, MAX_PLAYERS)
	print(peer)
	network_peer = peer
	print("(server-test) Server started at port ", SERVER_PORT)

	# Get peer ID (server has always 1, clients higher)
	#var peer_id = get_network_unique_id()

	# No longer accept any connections
	#set_refuse_new_network_connections(true)
	# Terminate the network
	#network_peer = null

	#quit()

func _peer_connected(id):
	print("(server-test) _peer_connected(", id, ")")

func _peer_disconnected(id):
	print("(server-test) _peer_disconnected(", id, ")")

