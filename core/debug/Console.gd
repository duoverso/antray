# Thanks to https://godotengine.org/qa/642/function-could-evaluate-strings-expressions-handle-variables
extends Node
class_name Console

onready var editor := $"%Editor"
onready var output := $"%Output"

static func eval(code: String):
	var script := GDScript.new()
	script.set_source_code("extends Node\nfunc execute():\n\t" + code.replace("\n", "\n\t"))
	var error := script.reload()
	if error != OK:
		printerr("error: ", error)
		return
	var obj = Node.new()
	obj.set_script(script)
	return obj.execute()

func _on_Execute_pressed() -> void:
	output.text += str(self.eval(editor.text)) + "\n"

func _on_Show_toggled(button_pressed: bool) -> void:
	output.visible = button_pressed

func _on_Copy_pressed() -> void:
	OS.clipboard = output.text

func _on_Clear_pressed() -> void:
	output.text = ""
